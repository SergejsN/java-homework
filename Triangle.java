package test;

import java.util.Scanner;

public class Triangle {

	public static void main(String[] args) {
		
		 Scanner length = new Scanner(System.in);
	
	        System.out.println("Enter length of the triangle : ");
	        int size = 0;
	        size = length.nextInt(); //User input
	        int i, j, k;
	        for (i = 0; i <= size; i++) { //Getting through all elements from user input
	        	for (j = i; j < size; j++) //Print out spaces
	        		System.out.print(" ");
	        	for(k = size; k< (i * 2);k++) // Prints out stars
	        		System.out.print("*");
	        	System.out.print("\n"); // starts every cycle from a new line
	        		
	          }
	}

}
