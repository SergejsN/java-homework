package test;

import java.util.Scanner;
import java.util.Stack;
 
public class Parentheses {
    public static void main(String[] args) {
 
        Scanner sc = new Scanner(System.in);  
        System.out.println("Enter brackets : ");
        String brackets = sc.nextLine(); //User Input
        
        
 
        Stack<Character> stack = new Stack<>();//Creating stack for brackets
        boolean isBalanced = true;// to check if brackets are balanced
 
        for (int i = 0; i < brackets.length(); i++) {//Getting through all elements from user input
            char bracket = brackets.charAt(i);//Getting one single bracket and putting it into char
            if (bracket == '{' || bracket == '[' || bracket == '(') {//Checking if bracket is opening type
                stack.push(bracket);//Adding the opening bracket to the stack
            } else {
                if (bracket == '}') {
                    bracket = '{';
                }  if (bracket == ')') {
                    bracket = '(';
                } else if (bracket == ']') {
                    bracket = '[';
                }
 
                if (stack.empty()) {//Checking if stack is empty
                    isBalanced = false;
                    break;
                }
 
                if (bracket == stack.peek()) {
                    stack.pop();
                } else {
                    isBalanced = false;
                    break;
                }
            }
        }
 
        if (isBalanced) {
            System.out.println("Balanced");
        } else {
            System.out.println("Is not balanced");
        }
    }
}