
public class Solution { 
	
	public static void main(String[] args) {
		int[] a = new int[]{1,5,5,5,6,7,7,7,7,7,7,7,7,5,5,5,5}; 
		int count = 0, max = 0;
		    
		    for (int i = 1; i < a.length; i++) { //Getting through all elements in array
		        if (a[i] == a[i - 1]) { //Check for recurring sequence in array 
		            count++;      
		        } else {
		            if (count > max) { 
		                max = count;     
		            }
		            count = 1;   
		        }
		        if (count == max) { //show the element with the most recurring sequence in array
	           	 System.out.println(a[i] + " has the longest sequence"); 	
	            }
	}
  }  
}
	  
